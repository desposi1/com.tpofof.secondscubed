package com.tpofof.secondscubed.cube;

public final class CubeMoveManagerFactory {

	private static final StandardCubeMoveManager standardManager = new StandardCubeMoveManager();
	private static final CubeMoveManager defaultManager = standardManager;
	
	private CubeMoveManagerFactory() { }
	
	public static StandardCubeMoveManager getStandardCubeMoveManager() {
		return standardManager;
	}
	
	public static CubeMoveManager getDefaultCubeMoveManager() {
		return defaultManager;
	}
}
