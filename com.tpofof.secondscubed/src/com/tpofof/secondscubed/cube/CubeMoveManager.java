package com.tpofof.secondscubed.cube;

import java.util.List;

import com.tpofof.secondscubed.shared.CubeMove;

public interface CubeMoveManager {

	CubeMove getRandomMove();
	
	List<CubeMove> getRandomMoveList(int length);
}
