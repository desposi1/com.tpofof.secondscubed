package com.tpofof.secondscubed.cube;

import static com.tpofof.secondscubed.shared.CubeMoveType.BACK;
import static com.tpofof.secondscubed.shared.CubeMoveType.DOWN;
import static com.tpofof.secondscubed.shared.CubeMoveType.FRONT;
import static com.tpofof.secondscubed.shared.CubeMoveType.LEFT;
import static com.tpofof.secondscubed.shared.CubeMoveType.RIGHT;
import static com.tpofof.secondscubed.shared.CubeMoveType.UP;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.tpofof.secondscubed.shared.CubeMove;
import com.tpofof.secondscubed.shared.CubeMoveType;

public class StandardCubeMoveManager implements CubeMoveManager {

	private final CubeMoveType[] supportedMoves;
	private final Random rand;
	
	public StandardCubeMoveManager() {
		this(System.currentTimeMillis());
	}
	
	public StandardCubeMoveManager(long seed) {
		supportedMoves = new CubeMoveType[]{ RIGHT, LEFT, FRONT, BACK, UP, DOWN };
		this.rand = new Random();
	}
	
	@Override
	public CubeMove getRandomMove() {
		CubeMoveType type = supportedMoves[getRandomNumber(0, supportedMoves.length-1)];
		boolean prime = false;
		boolean doubleMove = false;
		if (getRandomBoolean()) {
			if (getRandomBoolean()) {
				prime = getRandomBoolean();
				doubleMove = !prime && getRandomBoolean();
			} else {
				doubleMove = getRandomBoolean();
				prime = !doubleMove && getRandomBoolean();
			}
		}
		return new CubeMove(type, prime, doubleMove);
	}

	@Override
	public List<CubeMove> getRandomMoveList(int length) {
		if (length < 1) {
			return Collections.emptyList();
		}
		List<CubeMove> moveList = new ArrayList<CubeMove>();
		CubeMove lastMove = getRandomMove();
		moveList.add(lastMove);
		while (moveList.size() < length) {
			CubeMove nextMove = lastMove;
			while (lastMove.getCubeMoveType() == nextMove.getCubeMoveType()) {
				nextMove = getRandomMove();
			}
			moveList.add(nextMove);
			lastMove = nextMove;
		}
		return moveList;
	}
	
	protected int getRandomNumber(int min, int maxInclusive) {
		int temp = rand.nextInt(maxInclusive + 1);
		return min + temp;
	}
	
	protected boolean getRandomBoolean() {
		return getRandomNumber(0, 1) == 1;
	}
}
