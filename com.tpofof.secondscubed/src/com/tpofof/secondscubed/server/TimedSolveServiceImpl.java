package com.tpofof.secondscubed.server;

import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.tpofof.secondscubed.client.services.TimedSolveService;
import com.tpofof.secondscubed.datastore.DatastoreManager;
import com.tpofof.secondscubed.datastore.DatastoreManagerFactory;
import com.tpofof.secondscubed.shared.TimedSolve;

public class TimedSolveServiceImpl extends RemoteServiceServlet implements TimedSolveService {

	private static final long serialVersionUID = 1L;
	
	private final DatastoreManager datastoreManager = DatastoreManagerFactory.getDefaultDatastoreManager();
	
	@Override
	public void save(TimedSolve ts) {
		if (datastoreManager.saveTimedSolve(ts) == null) {
			System.err.println("Error saving timed solve: " + ts);
		}
	}

	@Override
	public void delete(TimedSolve ts) {
		if (datastoreManager.deleteTimedSolve(ts) == null) {
			System.err.println("Error deleting timed solve: " + ts);
		}
	}

	@Override
	public List<TimedSolve> get(String userEmail) {
		return datastoreManager.getTimedSolvesByUserEmail(userEmail);
	}

}
