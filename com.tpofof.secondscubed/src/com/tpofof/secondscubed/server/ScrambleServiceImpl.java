package com.tpofof.secondscubed.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.tpofof.secondscubed.client.services.ScrambleService;
import com.tpofof.secondscubed.cube.CubeMoveManager;
import com.tpofof.secondscubed.cube.CubeMoveManagerFactory;

public class ScrambleServiceImpl extends RemoteServiceServlet implements
		ScrambleService {

	private static final long serialVersionUID = 1L;
	
	private final CubeMoveManager cubeMoveManager = CubeMoveManagerFactory.getDefaultCubeMoveManager();

	@Override
	public String getScramble(int length) {
		return cubeMoveManager.getRandomMoveList(length).toString();
	}

}
