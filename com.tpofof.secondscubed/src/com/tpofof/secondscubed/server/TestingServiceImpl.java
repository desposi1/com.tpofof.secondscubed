package com.tpofof.secondscubed.server;

import java.util.Map;
import java.util.concurrent.Callable;

import com.google.common.collect.Maps;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.tpofof.secondscubed.client.services.TestingService;
import com.tpofof.secondscubed.shared.TestResult;

public class TestingServiceImpl extends RemoteServiceServlet implements TestingService {

	private static final long serialVersionUID = 1L;
	
	protected Map<String, TestExecutor> executors;
	
	public TestingServiceImpl() {
		this.executors = Maps.newHashMap();
		initExecutors();
	}

	@Override
	public TestResult test(String testName) {
		try {
			return executors.containsKey(testName)
					? TestResult.create(executors.get(testName).call.call())
					: TestResult.createError("Test with name \"" + testName + "\" not found");
		} catch (Exception e) {
			return TestResult.createError("Failed to run test: " + e.getMessage());
		}
	}

	@Override
	public TestResult test(String testName, String params) {
		return TestResult.createError("Not implemented");
	}

	protected static class TestExecutor {
		public String name;
		public Callable<String> call;
		public TestExecutor(String name, Callable<String> call) {
			super();
			this.name = name;
			this.call = call;
		}
	}
	
	protected void initExecutors() {
		executors.put("test", new TestExecutor("test", new Callable<String>() {
			@Override
			public String call() throws Exception {
				return "test run successfully";
			}
		}));
	}
}
