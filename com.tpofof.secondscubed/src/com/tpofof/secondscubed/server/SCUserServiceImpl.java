package com.tpofof.secondscubed.server;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.tpofof.secondscubed.client.services.SCUserService;
import com.tpofof.secondscubed.datastore.DatastoreManager;
import com.tpofof.secondscubed.datastore.DatastoreManagerFactory;
import com.tpofof.secondscubed.shared.SCUser;

public class SCUserServiceImpl extends RemoteServiceServlet implements
		SCUserService {

	private static final long serialVersionUID = 1L;
	
	private final UserService userService = UserServiceFactory.getUserService();
	private final DatastoreManager datastoreManager = DatastoreManagerFactory.getDefaultDatastoreManager();

	@Override
	public SCUser getLoggedInUser() {
		User user = userService.getCurrentUser();
		SCUser scUser = user == null ? null : new SCUser(user.getNickname(), user.getEmail());
		if (scUser != null && datastoreManager.getUser(scUser.getEmail()) == null) {
			datastoreManager.saveUser(scUser);
		}
		return scUser;
	}

	@Override
	public String getLoginLink(String redirectUrl) {
		return userService.createLoginURL(redirectUrl);
	}

	@Override
	public String getLogoutLink(String redirectUrl) {
		return userService.createLogoutURL(redirectUrl);
	}

}
