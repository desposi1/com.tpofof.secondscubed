package com.tpofof.secondscubed.shared;

import java.io.Serializable;
import java.util.Collections;
import java.util.Set;

public class TimedSolve implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long key;
	private String userEmail;
	private long startTime;
	private long endTime;
	private long diff;
	private String scramble;
	private Set<String> tags;
	
	@SuppressWarnings("unused")
	private TimedSolve() { }
	
	public TimedSolve(long startTime, long endTime, String userEmail, String scramble) {
		this(startTime, endTime, userEmail, scramble, Collections.<String>emptySet());
	}
	
	public TimedSolve(long startTime, long endTime, String userEmail, String scramble, Set<String> tags) {
		this(null, userEmail, startTime, endTime, endTime, scramble, tags);
	}
	
	public TimedSolve(Long key, TimedSolve copy) {
		this(key, copy.userEmail, copy.startTime, copy.endTime, copy.diff, copy.scramble, copy.tags);
	}
	
	public TimedSolve(Long key, String userEmail, long startTime, long endTime,
			long diff, String scramble, Set<String> tags) {
		super();
		this.key = key;
		this.userEmail = userEmail;
		this.startTime = startTime;
		this.endTime = endTime;
		this.diff = diff;
		this.scramble = scramble;
		this.tags = tags;
	}

	public Long getKey() {
		return key;
	}
	
	public String getUserEmail() {
		return userEmail;
	}

	public long getStartTime() {
		return startTime;
	}

	public long getEndTime() {
		return endTime;
	}
	
	public long getDiff() { 
		return diff;
	}

	public String getScramble() {
		return scramble;
	}
	
	public Set<String> getTags() {
		return tags;
	}

	@Override
	public String toString() {
		return "TimedSolve [userEmail=" + userEmail + ", startTime="
				+ startTime + ", endTime=" + endTime + ", diff=" + diff
				+ ", scramble=" + scramble + ", tags=" + tags + "]";
	}
}
