package com.tpofof.secondscubed.shared;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;

public final class TextManager {

	public static final DateTimeFormat DAY_TIME_FORMAT = DateTimeFormat
			.getFormat("EEE hh:mm a");
	public static final DateTimeFormat FULL_DAY_TIME_FORMAT = DateTimeFormat
			.getFormat("EEE hh:mm:ss a");

	private TextManager() {
	}

	public static String formatToDayTimeFormat(long time) {
		return formatToDayTimeFormat(new Date(time));
	}

	public static String formatToDayTimeFormat(Date date) {
		return DAY_TIME_FORMAT.format(date);
	}

	public static String formatToFullDayTimeFormat(long time) {
		return formatToFullDayTimeFormat(new Date(time));
	}

	public static String formatToFullDayTimeFormat(Date date) {
		return FULL_DAY_TIME_FORMAT.format(date);
	}

	public static String formatToSolveTime(long diff) {
		long hours = getHours(diff);
		long minutes = getMinutes(diff);
		long seconds = getSeconds(diff);
		long millis = getMillis(diff);

		String text = "";
		if (hours > 0) {
			text = pad(2, hours) + ":" + pad(2, minutes) + ":"
					+ pad(2, seconds) + "." + pad(3, millis);
		} else if (minutes > 0) {
			text = pad(2, minutes) + ":" + pad(2, seconds) + "."
					+ pad(3, millis);
		} else {
			text = pad(2, seconds) + "." + pad(3, millis);
		}
		return text;
	}

	private static String pad(int length, long num) {
		String text = num + "";
		while (text.length() < length) {
			text = '0' + text;
		}
		return text;
	}

	private static long getHours(long diff) {
		return (diff / 3600000);
	}

	private static long getMinutes(long diff) {
		return (diff / 60000) % 60;
	}

	private static long getSeconds(long diff) {
		return (diff / 1000) % 60;
	}

	private static long getMillis(long diff) {
		return diff % 1000;
	}
}
