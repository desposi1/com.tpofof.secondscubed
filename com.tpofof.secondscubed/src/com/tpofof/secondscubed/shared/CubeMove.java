package com.tpofof.secondscubed.shared;

import java.io.Serializable;

public final class CubeMove implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private final CubeMoveType cubeMoveType;
	private final boolean prime;
	// will always be 1 or 2
	private final boolean doubleMove;
	
	public CubeMove(CubeMoveType cubeMoveType, boolean prime, boolean doubleMove) {
		super();
		this.cubeMoveType = cubeMoveType;
		this.prime = prime;
		this.doubleMove = doubleMove;
	}

	public CubeMoveType getCubeMoveType() {
		return cubeMoveType;
	}

	public boolean isPrime() {
		return prime;
	}

	public boolean isDoubleMove() {
		return doubleMove;
	}

	@Override
	public String toString() {
		return cubeMoveType.getSequenceName() + (prime ? "'" : doubleMove ? "2" : "");
	}
}
