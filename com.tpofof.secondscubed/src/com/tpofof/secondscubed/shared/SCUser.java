package com.tpofof.secondscubed.shared;

import java.io.Serializable;

public class SCUser implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long key;
	private String email;
	private String nickname;
	
	@SuppressWarnings("unused")
	private SCUser() { 
		this("", "");
	}
	
	public SCUser(String nickname, String email) {
		this(nickname, email, null);
	}
	
	public SCUser(String nickname, String email, Long key) {
		super();
		this.nickname = nickname;
		this.email = email;
		this.key = key;
	}
	
	public Long getKey() {
		return key;
	}

	public String getNickname() {
		return nickname;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String toString() {
		return "SCUser [nickname=" + nickname + ", email=" + email + "]";
	}
}
