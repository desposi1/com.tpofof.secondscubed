package com.tpofof.secondscubed.shared;

import java.io.Serializable;

public class TestResult implements Serializable {

	private static final long serialVersionUID = 1L;

	private String message;
	private String errorMessage;
	
	@SuppressWarnings("unused")
	private TestResult() { }
	
	protected TestResult(String message, String errorMessage) {
		this.message = message;
		this.errorMessage = errorMessage;
	}
	
	public String getMessage() {
		return message;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
	
	public boolean hasError() {
		return errorMessage != null;
	}

	public static TestResult create(String message) {
		return new TestResult(message, null);
	}
	
	public static TestResult createError(String errorMessage) {
		return new TestResult(null, errorMessage);
	}
}
