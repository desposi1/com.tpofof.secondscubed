package com.tpofof.secondscubed.shared;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum CubeMoveType implements Serializable {

	RIGHT("Right", "R"),
	LEFT("Left", "L"),
	FRONT("Front", "F"),
	BACK("Back", "B"),
	UP("Up", "U"),
	DOWN("DOWN", "D"),
	UNKNOWN("Unknown", "X");
	
	private static final Map<String, CubeMoveType> lookupMap;
	
	static {
		Map<String, CubeMoveType> temp = new HashMap<String, CubeMoveType>();
		temp.put(RIGHT.sequenceString, RIGHT);
		temp.put(RIGHT.verboseString, RIGHT);
		temp.put(LEFT.sequenceString, LEFT);
		temp.put(LEFT.verboseString, LEFT);
		temp.put(FRONT.sequenceString, FRONT);
		temp.put(FRONT.verboseString, FRONT);
		temp.put(BACK.sequenceString, BACK);
		temp.put(BACK.verboseString, BACK);
		temp.put(UP.sequenceString, UP);
		temp.put(UP.verboseString, UP);
		temp.put(DOWN.sequenceString, DOWN);
		temp.put(DOWN.verboseString, DOWN);
		lookupMap = Collections.unmodifiableMap(temp);
	}
	
	private final String verboseString;
	private final String sequenceString;
	
	private CubeMoveType(String verboseString, String sequenceString) {
		this.verboseString = verboseString;
		this.sequenceString = sequenceString;
	}
	
	public String getVerboseName() {
		return verboseString;
	}
	
	public String getSequenceName() {
		return sequenceString;
	}
	
	public static CubeMoveType lookup(String lookupName) {
		return lookupMap.containsKey(lookupName)
				? lookupMap.get(lookupName)
				: UNKNOWN;
	}
}
