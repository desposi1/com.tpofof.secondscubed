package com.tpofof.secondscubed.client.header;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.tpofof.secondscubed.client.services.SCUserService;
import com.tpofof.secondscubed.client.services.SCUserServiceAsync;
import com.tpofof.secondscubed.shared.SCUser;

public class AuthenticationDisplay extends Composite {

	private final SCUserServiceAsync userService = GWT.create(SCUserService.class);
	@SuppressWarnings("unused")
	private final EventBus eventBus;
	
	public AuthenticationDisplay(final EventBus eventBus) {
		final HorizontalPanel panel = new HorizontalPanel();
		this.eventBus = eventBus;
		
		userService.getLoggedInUser(new AsyncCallback<SCUser>() {
			@Override
			public void onSuccess(SCUser result) {
				if (result != null) {
					Label greeting = new Label("Hello, " + result.getNickname() + "!");
					panel.add(greeting);
					getLogoutLink(panel);
				} else {
					getLoginLink(panel);
				}
			}
			@Override
			public void onFailure(Throwable caught) {
				// TODO failure getting logged in user
			}
		});
		
		initWidget(panel);
	}
	
	protected void getLoginLink(final HorizontalPanel panel) {
		userService.getLoginLink(getRedirectUrl(), new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				SimplePanel divPanel = new SimplePanel();
				divPanel.add(new Anchor("login", result));
				panel.add(divPanel);
			}
			@Override
			public void onFailure(Throwable caught) {
				// TODO: failure getting login link
			}
		});
	}
	
	protected String getRedirectUrl() {
		return GWT.getHostPageBaseURL() + (GWT.isProdMode() ? "" : "?gwt.codesvr=127.0.0.1:9997");
	}
	
	protected void getLogoutLink(final HorizontalPanel panel) {
		userService.getLogoutLink(getRedirectUrl(), new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				SimplePanel divPanel = new SimplePanel();
				divPanel.add(new Anchor("logout", result));
				panel.add(divPanel);
			}
			@Override
			public void onFailure(Throwable caught) {
				// TODO: failure getting logout link
			}
		});
	}
}
