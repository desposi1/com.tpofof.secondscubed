package com.tpofof.secondscubed.client.header;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;

public class HeaderDisplay extends Composite {
	
	public HeaderDisplay(EventBus eventBus) {
		SimplePanel authPanel = new SimplePanel();
		authPanel.addStyleName("pull-right");
		authPanel.addStyleName("col-md-4");
		authPanel.addStyleName("text-right");
		authPanel.add(new AuthenticationDisplay(eventBus));
		initWidget(authPanel);
	}
}
