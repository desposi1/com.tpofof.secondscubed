package com.tpofof.secondscubed.client.test;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.tpofof.secondscubed.client.feedback.FeedbackEvent;
import com.tpofof.secondscubed.client.feedback.FeedbackType;
import com.tpofof.secondscubed.client.services.TestingService;
import com.tpofof.secondscubed.client.services.TestingServiceAsync;
import com.tpofof.secondscubed.shared.TestResult;

public class TestForm extends Composite {

	private final TestingServiceAsync testingService = GWT.create(TestingService.class);
	
	private final TextBox commandTextBox;
	private final Button executeButton;
	
	public TestForm(final EventBus eventBus) {
		this.commandTextBox = new TextBox();
		this.executeButton = new Button("execute");
		executeButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String command = commandTextBox.getText();
				testingService.test(command, new AsyncCallback<TestResult>() {
					@Override
					public void onSuccess(TestResult result) {
						if (!result.hasError()) {
							eventBus.fireEvent(new FeedbackEvent(result.getMessage(), 
									FeedbackType.SUCCESS));
						} else {
							eventBus.fireEvent(new FeedbackEvent("Failed to execute event: " + 
									result.getErrorMessage(), FeedbackType.ERROR));
						}
					}
					@Override
					public void onFailure(Throwable caught) {
						eventBus.fireEvent(new FeedbackEvent("Communication Error: " + 
								caught.getMessage(), FeedbackType.ERROR, 20000));
					}
				});
			}
		});
		HorizontalPanel horPanel = new HorizontalPanel();
		horPanel.add(commandTextBox);
		horPanel.add(executeButton);
		initWidget(horPanel);
	}
}
