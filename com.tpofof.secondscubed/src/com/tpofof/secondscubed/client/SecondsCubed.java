package com.tpofof.secondscubed.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.ui.RootPanel;
import com.tpofof.secondscubed.client.feedback.FeedbackDisplay;
import com.tpofof.secondscubed.client.header.HeaderDisplay;
import com.tpofof.secondscubed.client.main.history.HistoryDisplay;
import com.tpofof.secondscubed.client.main.scramble.ScrambleProvider;
import com.tpofof.secondscubed.client.main.stats.StatsDisplay;
import com.tpofof.secondscubed.client.main.timer.TimerDisplay;
import com.tpofof.secondscubed.client.test.TestForm;

public class SecondsCubed implements EntryPoint {
	
	public void onModuleLoad() {
		EventBus eventBus = GWT.create(SimpleEventBus.class);
		
		HeaderDisplay headerDisplay = new HeaderDisplay(eventBus);
		RootPanel.get("sc-header").add(headerDisplay);
		
		FeedbackDisplay feedbackDisplay = new FeedbackDisplay(eventBus);
		RootPanel.get("sc-feedback").add(feedbackDisplay);

		ScrambleProvider scrambleProvider = new ScrambleProvider(eventBus);
		RootPanel.get("sc-scramble").add(scrambleProvider);

		TimerDisplay timer = new TimerDisplay(eventBus);
		RootPanel.get("sc-timer").add(timer);
		
		HistoryDisplay history = new HistoryDisplay(eventBus);
		RootPanel.get("sc-history").add(history);
		
		StatsDisplay stats = new StatsDisplay(eventBus);
		RootPanel.get("sc-stats").add(stats);
		
		if (!GWT.isProdMode()) {
			TestForm testForm = new TestForm(eventBus);
			RootPanel.get("sc-testing").add(testForm);
		}
	}
}
