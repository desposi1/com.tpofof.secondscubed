package com.tpofof.secondscubed.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("scramble")
public interface ScrambleService extends RemoteService {

	String getScramble(int length);
}
