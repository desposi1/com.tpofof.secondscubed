package com.tpofof.secondscubed.client.services;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.tpofof.secondscubed.shared.TimedSolve;

@RemoteServiceRelativePath("timedsolves")
public interface TimedSolveService extends RemoteService {

	void save(TimedSolve ts);
	
	void delete(TimedSolve ts);
	
	List<TimedSolve> get(String userEmail);
}
