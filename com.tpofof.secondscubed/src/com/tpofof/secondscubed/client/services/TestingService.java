package com.tpofof.secondscubed.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.tpofof.secondscubed.shared.TestResult;

@RemoteServiceRelativePath("testing")
public interface TestingService extends RemoteService {

	TestResult test(String testName);
	
	TestResult test(String testName, String params);
}
