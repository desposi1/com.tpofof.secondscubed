package com.tpofof.secondscubed.client.services;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.tpofof.secondscubed.shared.TimedSolve;

public interface TimedSolveServiceAsync {

	void delete(TimedSolve ts, AsyncCallback<Void> callback);

	void get(String userEmail, AsyncCallback<List<TimedSolve>> callback);

	void save(TimedSolve ts, AsyncCallback<Void> callback);

}
