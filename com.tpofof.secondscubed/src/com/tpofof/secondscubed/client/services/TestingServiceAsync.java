package com.tpofof.secondscubed.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.tpofof.secondscubed.shared.TestResult;

public interface TestingServiceAsync {

	void test(String testName, AsyncCallback<TestResult> callback);

	void test(String testName, String params, AsyncCallback<TestResult> callback);

}
