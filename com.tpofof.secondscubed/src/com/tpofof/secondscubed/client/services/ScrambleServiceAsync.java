package com.tpofof.secondscubed.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ScrambleServiceAsync {

	void getScramble(int length, AsyncCallback<String> callback);

}
