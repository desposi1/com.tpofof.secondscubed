package com.tpofof.secondscubed.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.tpofof.secondscubed.shared.SCUser;

public interface SCUserServiceAsync {

	void getLoggedInUser(AsyncCallback<SCUser> callback);

	void getLoginLink(String redirectUrl, AsyncCallback<String> callback);

	void getLogoutLink(String redirectUrl, AsyncCallback<String> callback);

}
