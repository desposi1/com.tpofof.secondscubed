package com.tpofof.secondscubed.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.tpofof.secondscubed.shared.SCUser;

@RemoteServiceRelativePath("user")
public interface SCUserService extends RemoteService {

	SCUser getLoggedInUser();
	
	String getLoginLink(String redirectUrl);
	
	String getLogoutLink(String redirectUrl);
}
