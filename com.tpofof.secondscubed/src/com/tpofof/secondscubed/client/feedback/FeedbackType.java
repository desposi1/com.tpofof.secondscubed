package com.tpofof.secondscubed.client.feedback;

public enum FeedbackType {

	INFO,
	SUCCESS,
	WARNING,
	ERROR;
}
