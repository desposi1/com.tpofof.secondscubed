package com.tpofof.secondscubed.client.feedback;

import com.google.gwt.event.shared.EventHandler;

public interface FeedbackEventHandler extends EventHandler {

	void onFeedback(FeedbackEvent event);
}
