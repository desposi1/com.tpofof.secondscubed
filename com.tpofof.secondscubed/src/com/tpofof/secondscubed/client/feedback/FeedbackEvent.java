package com.tpofof.secondscubed.client.feedback;

import com.google.gwt.event.shared.GwtEvent;

public class FeedbackEvent extends GwtEvent<FeedbackEventHandler> {

	public static final Type<FeedbackEventHandler> TYPE = new Type<FeedbackEventHandler>();
	
	private final String message;
	private final FeedbackType feedbackType;
	private final int msToLive;
	
	public FeedbackEvent(String message, FeedbackType feedbackType) {
		this(message, feedbackType, FeedbackDisplay.DEFAULT_FEEDBACK_TIME_TO_LIVE);
	}
	
	public FeedbackEvent(String message, FeedbackType feedbackType, int msToLive) {
		super();
		this.message = message;
		this.feedbackType = feedbackType;
		this.msToLive = msToLive;
	}

	public String getMessage() {
		return message;
	}

	public FeedbackType getFeedbackType() {
		return feedbackType;
	}
	
	public int getMsToLive() {
		return msToLive;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<FeedbackEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(FeedbackEventHandler handler) {
		handler.onFeedback(this);
	}

}
