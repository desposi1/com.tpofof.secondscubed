package com.tpofof.secondscubed.client.feedback;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class FeedbackDisplay extends VerticalPanel implements FeedbackEventHandler {

	public static final int DEFAULT_FEEDBACK_TIME_TO_LIVE = 3000;
	
	public FeedbackDisplay(EventBus eventBus) {
		super();
		this.setWidth("100%");
		eventBus.addHandler(FeedbackEvent.TYPE, this);
	}

	@Override
	public void onFeedback(FeedbackEvent event) {
		switch (event.getFeedbackType()) {
		case ERROR:
			error(event.getMessage(), event.getMsToLive());
			break;
		case INFO:
			info(event.getMessage(), event.getMsToLive());
			break;
		case SUCCESS:
			success(event.getMessage(), event.getMsToLive());
			break;
		case WARNING:
			warning(event.getMessage(), event.getMsToLive());
			break;
		}
	}
	
	public void info(String message) {
		info(message, DEFAULT_FEEDBACK_TIME_TO_LIVE);
	}
	
	public void info(String message, int feedbackTimeout) {
		feedback(message, "info", feedbackTimeout);
	}
	
	public void success(String message) {
		success(message, DEFAULT_FEEDBACK_TIME_TO_LIVE);
	}
	
	public void success(String message, int feedbackTimeout) {
		feedback(message, "success", feedbackTimeout);
	}
	
	public void warning(String message) {
		warning(message, DEFAULT_FEEDBACK_TIME_TO_LIVE);
	}
	
	public void warning(String message, int feedbackTimeout) {
		feedback(message, "warning", feedbackTimeout);
	}
	
	public void error(String message) {
		error(message, DEFAULT_FEEDBACK_TIME_TO_LIVE);
	}
	
	public void error(String message, int feedbackTimeout) {
		feedback(message, "danger", feedbackTimeout);
	}
	
	private void feedback(String message, String alertType, int feedbackTimeout) {
		final SimplePanel panel = new SimplePanel();
		panel.addStyleName("alert");
		panel.addStyleName("alert-dismissable");
		panel.addStyleName("alert-" + alertType);
		panel.getElement().setInnerHTML("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>");
		Label label = new Label();
		label.setText(message);
		Timer t = new Timer() {
			@Override
			public void run() {
				panel.setVisible(false);
			}
		};
		t.schedule(feedbackTimeout);
		panel.getElement().appendChild(label.getElement());
		add(panel);
	}
}
