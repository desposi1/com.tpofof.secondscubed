package com.tpofof.secondscubed.client.main.timer;

import com.google.gwt.event.shared.GwtEvent;

public class TimedSolveEvent extends GwtEvent<TimedSolveEventHandler> {

	public static final Type<TimedSolveEventHandler> TYPE = new Type<TimedSolveEventHandler>();
	
	private final long startTime;
	private final long endTime;
	private final long diff;
	private final String scramble;
	
	public TimedSolveEvent(long startTime, long endTime, String scramble) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.diff = endTime - startTime;
		this.scramble = scramble;
	}
	
	public long getStartTime() {
		return startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public long getDiff() {
		return diff;
	}

	public String getScramble() {
		return scramble;
	}

	@Override
	public Type<TimedSolveEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(TimedSolveEventHandler handler) {
		handler.onTimedSolve(this);
	}

}
