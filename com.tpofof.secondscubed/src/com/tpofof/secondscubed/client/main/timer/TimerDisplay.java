package com.tpofof.secondscubed.client.main.timer;

import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.tpofof.secondscubed.client.main.scramble.NewScrambleEvent;
import com.tpofof.secondscubed.client.main.scramble.NewScrambleEventHandler;
import com.tpofof.secondscubed.client.util.DialogActionEvent;
import com.tpofof.secondscubed.client.util.DialogActionEventHandler;
import com.tpofof.secondscubed.shared.TextManager;

public class TimerDisplay extends Composite implements DialogActionEventHandler {
	
	private static enum TimerState {
		STOPPED,
		STARTED,
		READY,
		DISABLED;
	}

	private final EventBus eventBus;
	private final Label timeLabel;
	private TimerState timerState;
	private TimerState previousTimerState;
	private long startTime;
	private long endTime;
	private Timer displayTimer;
	private String scramble;
	
	public TimerDisplay(EventBus eventBus) {
		super();
		this.eventBus = eventBus;
		
		this.timeLabel = new Label();
		SimplePanel panel = new SimplePanel();
		panel.addStyleName("bordered");
		panel.addStyleName("text-center");
		panel.add(timeLabel);
		
		this.startTime = System.currentTimeMillis();
		this.endTime = startTime;
		this.timerState = TimerState.READY;
		
		RootPanel rootPanel = RootPanel.get();
		rootPanel.addDomHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == ' ' && timerState == TimerState.READY) {
					start();
				} else if (event.getNativeKeyCode() == ' ' && timerState == TimerState.STOPPED) {
					timerState = TimerState.READY;
				}
			}
		}, KeyUpEvent.getType());
		
		rootPanel.addDomHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == ' ' && timerState == TimerState.STARTED) {
					stop();
				} else if (event.getNativeKeyCode() == ' ' && timerState == TimerState.READY) {
					updateTime(0);
				}
			}
		}, KeyDownEvent.getType());
		
		eventBus.addHandler(NewScrambleEvent.TYPE, new NewScrambleEventHandler() {
			@Override
			public void onNewScramble(NewScrambleEvent event) {
				scramble = event.getScramble();
			}
		});
		updateTimeRelative(endTime);
		
		eventBus.addHandler(DialogActionEvent.TYPE, this);
		
		initWidget(panel);
	}
	
	protected void start() {
		timerState = TimerState.STARTED;
		displayTimer = new Timer() {
			@Override
			public void run() {
				if (timerState == TimerState.STARTED || previousTimerState == TimerState.STARTED) {
					updateTime();
				}
			}
		};
		startTime = System.currentTimeMillis();
		displayTimer.scheduleRepeating(27);
	}
	
	protected void stop() {
		endTime = System.currentTimeMillis();
		int diff = (int)(endTime - startTime);
		if (diff > 250) {
			updateTime(diff);
			displayTimer.cancel();
			displayTimer = null;
			timerState = TimerState.STOPPED;
			previousTimerState = TimerState.STOPPED;
			TimedSolveEvent solveEvent = new TimedSolveEvent(startTime, endTime, scramble);
			eventBus.fireEvent(solveEvent);
		} else {
			updateTime(0);
			timerState = TimerState.READY;
		}
	}
	
	protected void updateTime() {
		updateTimeRelative(System.currentTimeMillis());
	}
	
	protected void updateTimeRelative(long endTime) {
		updateTime(endTime - startTime);
	}
	
	protected void updateTime(long diff) {
		timeLabel.setText(TextManager.formatToSolveTime(diff));
	}

	@Override
	public void onDialogActionEvent(DialogActionEvent event) {
		switch (event.getActionType()) {
		case OPEN:
			previousTimerState = timerState;
			timerState = TimerState.DISABLED;
			break;
		case CLOSE:
			timerState = previousTimerState;
			break;
		}
	}
}
