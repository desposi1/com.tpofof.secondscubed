package com.tpofof.secondscubed.client.main.timer;

import com.google.gwt.event.shared.EventHandler;

public interface TimedSolveEventHandler extends EventHandler {

	void onTimedSolve(TimedSolveEvent event);
}
