package com.tpofof.secondscubed.client.main.scramble;

import com.google.gwt.event.shared.GwtEvent;

public class NewScrambleEvent extends GwtEvent<NewScrambleEventHandler> {

	public static final Type<NewScrambleEventHandler> TYPE = new Type<NewScrambleEventHandler>(); 
	
	private final String scramble;
	
	public NewScrambleEvent(String scramble) {
		super();
		this.scramble = scramble;
	}

	public String getScramble() {
		return scramble;
	}

	@Override
	public Type<NewScrambleEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(NewScrambleEventHandler handler) {
		handler.onNewScramble(this);
	}

}
