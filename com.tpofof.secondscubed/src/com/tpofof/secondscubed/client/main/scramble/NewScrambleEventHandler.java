package com.tpofof.secondscubed.client.main.scramble;

import com.google.gwt.event.shared.EventHandler;

public interface NewScrambleEventHandler extends EventHandler {

	void onNewScramble(NewScrambleEvent event);
}
