package com.tpofof.secondscubed.client.main.scramble;

import static com.google.gwt.user.client.ui.HasVerticalAlignment.ALIGN_MIDDLE;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.tpofof.secondscubed.client.feedback.FeedbackEvent;
import com.tpofof.secondscubed.client.feedback.FeedbackType;
import com.tpofof.secondscubed.client.services.ScrambleService;
import com.tpofof.secondscubed.client.services.ScrambleServiceAsync;

public class ScrambleProvider extends Composite {

	private static final int DEFAULT_SCRAMBLE_LENGTH = 20;
	private static final int MAX_SCRAMBLE_LENGTH = 75;
	
	private final ScrambleServiceAsync scrambleService = GWT.create(ScrambleService.class);

	private final EventBus eventBus;
	private final TextBox scrambleLengthTextBox;
	private final Button rescrambleButton;
	private final Label scrambleDisplayLabel;
	private final HorizontalPanel horPanel;
	private int length = DEFAULT_SCRAMBLE_LENGTH;
	
	public ScrambleProvider(EventBus eventBus) {
		this.eventBus = eventBus;

		final SimplePanel formPanel = new SimplePanel();
		formPanel.addStyleName("form-group");
		formPanel.addStyleName("middle-aligned");
		formPanel.addStyleName("padded");
		
		this.scrambleLengthTextBox = new TextBox();
		scrambleLengthTextBox.setText("" + length);
		scrambleLengthTextBox.setWidth("35px");
		scrambleLengthTextBox.setHeight("40px");
		scrambleLengthTextBox.addStyleName("form-control");
		scrambleLengthTextBox.addBlurHandler(new BlurHandler() {
			@Override
			public void onBlur(BlurEvent event) {
				String currentText = scrambleLengthTextBox.getText();
				try {
					int newLength = Integer.parseInt(currentText);
					length = newLength > MAX_SCRAMBLE_LENGTH ? MAX_SCRAMBLE_LENGTH : newLength;
					formPanel.removeStyleName("has-error");
				} catch (NumberFormatException e) {
					formPanel.addStyleName("has-error");
				}
			}
		});
		
		
		this.rescrambleButton = new Button("Rescramble", new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				resetScramble(true);
			}
		});
		rescrambleButton.addStyleName("btn");
		rescrambleButton.addStyleName("btn-default");
		rescrambleButton.setHeight("40px");
		
		this.scrambleDisplayLabel = new Label();
		resetScramble(false);
		
		this.horPanel = new HorizontalPanel();
		horPanel.addStyleName("padded-table");
		horPanel.setVerticalAlignment(ALIGN_MIDDLE);
		horPanel.add(scrambleLengthTextBox);
		horPanel.add(rescrambleButton);
		horPanel.add(scrambleDisplayLabel);
		
		formPanel.add(horPanel);
		
		initWidget(formPanel);
	}
	
	private void resetScramble(final boolean displayError) {
		scrambleService.getScramble(length, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				scrambleDisplayLabel.setText(result);
				eventBus.fireEvent(new NewScrambleEvent(result));
			}
			@Override
			public void onFailure(Throwable caught) {
				// TODO retry?
				if (displayError) {
					eventBus.fireEvent(new FeedbackEvent("Could not get new scramble from the server.", 
							FeedbackType.ERROR));
				}
			}
		});
	}
}
