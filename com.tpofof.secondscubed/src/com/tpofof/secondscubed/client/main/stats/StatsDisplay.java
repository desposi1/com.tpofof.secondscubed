package com.tpofof.secondscubed.client.main.stats;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.tpofof.secondscubed.client.main.history.HistoryUpdateEvent;
import com.tpofof.secondscubed.client.main.history.HistoryUpdateEventHandler;
import com.tpofof.secondscubed.client.main.timer.TimedSolveEvent;
import com.tpofof.secondscubed.shared.TextManager;

public class StatsDisplay extends Composite {
	
	protected static final int ROW_FASTEST = 0;
	protected static final int ROW_BEST_5 = 1;
	protected static final int ROW_BEST_10 = 2;
	protected static final int ROW_AVG = 3;
	
	private final FlexTable statsTable;
	
	public StatsDisplay(EventBus eventBus) {
		SimplePanel titlePanel = new SimplePanel();
		HeadingElement titleHeader = Document.get().createHElement(2);
		titleHeader.setInnerText("Recent Solves Statistics");
		titlePanel.getElement().appendChild(titleHeader);
		
		statsTable = new FlexTable();
		statsTable.addStyleName("bordered-td");
		statsTable.addStyleName("padded-table");
		statsTable.setText(ROW_FASTEST, 0, "Fastest Solve");
		statsTable.setText(ROW_BEST_5, 0, "Average Best 5");
		statsTable.setText(ROW_BEST_10, 0, "Average Best 10");
		statsTable.setText(ROW_AVG, 0, "Average Solve");
		for(int i=0;i<4;i++) {
			statsTable.getCellFormatter().getElement(i, 0).addClassName("table-header");
		}
		
		eventBus.addHandler(HistoryUpdateEvent.TYPE, new HistoryUpdateEventHandler() {
			@Override
			public void onHistoryUpdate(HistoryUpdateEvent event) {
				update(event.getCurrentHistory());
			}
		});
		init();
		
		VerticalPanel mainPanel = new VerticalPanel();
		mainPanel.add(titlePanel);
		mainPanel.add(statsTable);
		initWidget(mainPanel);
	}
	
	protected void init() {
		String zeroedTime = "00.000";
		statsTable.setText(ROW_FASTEST, 1, zeroedTime);
		statsTable.setText(ROW_BEST_5, 1, zeroedTime);
		statsTable.setText(ROW_BEST_10, 1, zeroedTime);
		statsTable.setText(ROW_AVG, 1, zeroedTime);
	}
	
	public void update(List<TimedSolveEvent> solveHistory) {
		if (solveHistory.isEmpty()) {
			init();
		} else {
			List<TimedSolveEvent> sortedHistory = Lists.newArrayList(solveHistory);
			Collections.sort(sortedHistory, new Comparator<TimedSolveEvent>() {
				@Override
				public int compare(TimedSolveEvent solve1, TimedSolveEvent solve2) {
					return (int)(solve1.getDiff() - solve2.getDiff());
				}
			});
			statsTable.setText(ROW_FASTEST, 1, TextManager.formatToSolveTime(sortedHistory.get(0).getDiff()));
			long avg5 = 0;
			long avg10 = 0;
			long avg = 0;
			int i = 0;
			for (TimedSolveEvent e : sortedHistory) {
				if (i < 5) {
					avg5 += e.getDiff();
				}
				if (i < 10) {
					avg10 += e.getDiff();
				}
				avg += e.getDiff();
				i++;
			}
			avg /= i;
			avg5 /= i < 5 ? i : 5;
			avg10 /= i < 10 ? i : 10;
			statsTable.setText(ROW_BEST_5, 1, TextManager.formatToSolveTime(avg5));
			statsTable.setText(ROW_BEST_10, 1, TextManager.formatToSolveTime(avg10));
			statsTable.setText(ROW_AVG, 1, TextManager.formatToSolveTime(avg));
		}
	}
}
