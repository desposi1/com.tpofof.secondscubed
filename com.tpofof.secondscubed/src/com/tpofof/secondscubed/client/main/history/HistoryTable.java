package com.tpofof.secondscubed.client.main.history;

import java.util.LinkedList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.tpofof.secondscubed.client.main.timer.TimedSolveEvent;
import com.tpofof.secondscubed.client.services.SCUserService;
import com.tpofof.secondscubed.client.services.SCUserServiceAsync;
import com.tpofof.secondscubed.client.services.TimedSolveService;
import com.tpofof.secondscubed.client.services.TimedSolveServiceAsync;
import com.tpofof.secondscubed.client.util.DialogActionType;
import com.tpofof.secondscubed.client.util.DialogActionEvent;
import com.tpofof.secondscubed.shared.SCUser;
import com.tpofof.secondscubed.shared.TextManager;
import com.tpofof.secondscubed.shared.TimedSolve;

public class HistoryTable extends Composite {

	private final TimedSolveServiceAsync timedSolveService = GWT.create(TimedSolveService.class);
	private final SCUserServiceAsync userService = GWT.create(SCUserService.class);
	
	private final EventBus eventBus;
	private final List<TimedSolveEvent> solveEvents;
	private final FlexTable table;
	
	public HistoryTable(EventBus eventBus) {
		this.eventBus = eventBus;
		
		this.solveEvents = new LinkedList<TimedSolveEvent>();
		
		this.table = new FlexTable();
		table.addStyleName("bordered-td");
		table.addStyleName("padded-table");
		table.setText(0, 0, "Start Time");
		table.setText(0, 1, "Solve Time");
		table.setText(0, 2, "");
		initWidget(table);
	}
	
	public List<TimedSolveEvent> getSolveHistory() {
		return ImmutableList.copyOf(solveEvents);
	}
	
	public void add(final TimedSolveEvent solveEvent) {
		solveEvents.add(solveEvent);
		int row = solveEvents.size();
		table.setText(row, 0, TextManager.formatToDayTimeFormat(solveEvent.getStartTime()));
		table.setText(row, 1, TextManager.formatToSolveTime(solveEvent.getDiff()));
		SimplePanel closeButton = new SimplePanel();
		closeButton.addStyleName("glyphicon");
		closeButton.addStyleName("glyphicon-floppy-remove");
		closeButton.addStyleName("clickable");
		closeButton.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				int row = solveEvents.indexOf(solveEvent) + 1;
				if (row > 0) {
					solveEvents.remove(row - 1);
					table.removeRow(row);
					eventBus.fireEvent(new HistoryUpdateEvent(getSolveHistory()));
					// TODO: DELETE EVENT
				}
			}
		}, ClickEvent.getType());
		SimplePanel infoButton = new SimplePanel();
		infoButton.addStyleName("glyphicon");
		infoButton.addStyleName("glyphicon-info-sign");
		infoButton.addStyleName("clickable");
		infoButton.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				eventBus.fireEvent(new DialogActionEvent(DialogActionType.OPEN));
				new TimedSolveDialog(eventBus, solveEvent).center();
			}
		}, ClickEvent.getType());
		final SimplePanel saveButton = new SimplePanel();
		saveButton.addStyleName("glyphicon");
		saveButton.addStyleName("glyphicon-floppy-disk");
		saveButton.addStyleName("clickable");
		saveButton.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int row = solveEvents.indexOf(solveEvent) + 1;
				if (row > 0) {
					final TimedSolveEvent e = solveEvents.get(row - 1);
					userService.getLoggedInUser(new AsyncCallback<SCUser>() {
						@Override
						public void onSuccess(SCUser result) {
							long startTime = e.getStartTime();
							long endTime = e.getEndTime();
							String scramble = e.getScramble();
							String userEmail = result.getEmail();
							timedSolveService.save(new TimedSolve(startTime, endTime, userEmail, scramble), new AsyncCallback<Void>() {
								@Override
								public void onSuccess(Void result) {
									SimplePanel.setVisible(saveButton.getElement(), false);
								}
								@Override
								public void onFailure(Throwable caught) {
									// TODO Auto-generated method stub
								}
							});
						}
						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
						}
					});
				}
			}
		}, ClickEvent.getType());
		
		HorizontalPanel buttonPanel = new HorizontalPanel();
		buttonPanel.add(saveButton);
		buttonPanel.add(closeButton);
		buttonPanel.add(infoButton);
		table.setWidget(row, 2, buttonPanel);
	}
}
