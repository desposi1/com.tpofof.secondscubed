package com.tpofof.secondscubed.client.main.history;

import com.google.gwt.event.shared.EventHandler;

public interface HistoryUpdateEventHandler extends EventHandler {

	void onHistoryUpdate(HistoryUpdateEvent event);
}
