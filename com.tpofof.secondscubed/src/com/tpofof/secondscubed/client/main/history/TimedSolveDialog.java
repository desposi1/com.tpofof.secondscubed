package com.tpofof.secondscubed.client.main.history;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.tpofof.secondscubed.client.main.timer.TimedSolveEvent;
import com.tpofof.secondscubed.client.util.DialogActionEvent;
import com.tpofof.secondscubed.client.util.DialogActionType;
import com.tpofof.secondscubed.shared.TextManager;

public class TimedSolveDialog extends DialogBox {

	public TimedSolveDialog(final EventBus eventBus, final TimedSolveEvent timedSolve) {
		setText("My First Dialog");
	      setAnimationEnabled(true);
	      setGlassEnabled(true);
		
		VerticalPanel contentPanel = new VerticalPanel();
		
		SimplePanel titleHeaderPanel = new SimplePanel();
		HeadingElement titleHeader = Document.get().createHElement(1);
		titleHeader.setInnerHTML("Timed Solve Information");
		titleHeaderPanel.getElement().appendChild(titleHeader);
		
		FlexTable infoTable = new FlexTable();
		infoTable.addStyleName("padded-table");
		int row = 0;
		infoTable.setText(row, 0, "Start Time");
		infoTable.setText(row++, 1, TextManager.formatToFullDayTimeFormat(timedSolve.getStartTime()));
		infoTable.setText(row, 0, "Duration");
		infoTable.setText(row++, 1, TextManager.formatToSolveTime(timedSolve.getDiff()));
		infoTable.setText(row, 0, "Scramble");
		infoTable.setText(row++, 1, timedSolve.getScramble());
		infoTable.setText(row, 0, "User Email");
		infoTable.setText(row++, 1, "Anonymous");
		for(int i=0;i<row;i++) {
			infoTable.getCellFormatter().getElement(i, 0).addClassName("table-header");
		}

//		SimplePanel tagsTitleHeaderPanel = new SimplePanel();
//		HeadingElement tagsTitleHeader = Document.get().createHElement(4);
//		tagsTitleHeader.setInnerHTML("Tags");
//		tagsTitleHeaderPanel.getElement().appendChild(tagsTitleHeader);
//		
//		TextArea tagsTextArea = new TextArea();
		
		HorizontalPanel buttonPanel = new HorizontalPanel();
		Button closeButton = new Button("Close");
		closeButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				TimedSolveDialog.this.hide();
				eventBus.fireEvent(new DialogActionEvent(DialogActionType.CLOSE));
			}
		});
//		Button saveButton = new Button("Save");
//		saveButton.addClickHandler(new ClickHandler() {
//			@Override
//			public void onClick(ClickEvent event) {
//				// TODO
//				TimedSolveDialog.this.hide();
//				eventBus.fireEvent(new DialogActionEvent(DialogActionType.CLOSE));
//				eventBus.fireEvent(new FeedbackEvent("Saved Timed Solve", FeedbackType.SUCCESS));
//			}
//		});
		buttonPanel.add(closeButton);
//		buttonPanel.add(saveButton);
		
		contentPanel.add(titleHeaderPanel);
		contentPanel.add(infoTable);
//		contentPanel.add(tagsTitleHeaderPanel);
//		contentPanel.add(tagsTextArea);
		contentPanel.add(buttonPanel);
		
		this.setWidget(contentPanel);
	}
}
