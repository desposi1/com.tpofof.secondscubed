package com.tpofof.secondscubed.client.main.history;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.tpofof.secondscubed.client.main.timer.TimedSolveEvent;
import com.tpofof.secondscubed.client.main.timer.TimedSolveEventHandler;

public class HistoryDisplay extends Composite {

	private final SimplePanel titlePanel;
	private final HistoryTable historyTable;
	private final VerticalPanel mainPanel;
	
	public HistoryDisplay(final EventBus eventBus) {
		this.titlePanel = new SimplePanel();
		HeadingElement titleHeader = Document.get().createHElement(2);
		titleHeader.setInnerText("Recent Solves");
		titlePanel.getElement().appendChild(titleHeader);
		
		this.historyTable = new HistoryTable(eventBus);
		historyTable.setWidth("100%");
		
		eventBus.addHandler(TimedSolveEvent.TYPE, new TimedSolveEventHandler() {
			@Override
			public void onTimedSolve(TimedSolveEvent event) {
				historyTable.add(event);
				eventBus.fireEvent(new HistoryUpdateEvent(historyTable.getSolveHistory()));
			}
		});
		
		this.mainPanel = new VerticalPanel();
		mainPanel.addStyleName("padded-table");
		mainPanel.setWidth("100%");
		mainPanel.add(titlePanel);
		mainPanel.add(historyTable);
		SimplePanel panel = new SimplePanel(mainPanel);
		panel.addStyleName("padded");
		initWidget(panel);
	}
}
