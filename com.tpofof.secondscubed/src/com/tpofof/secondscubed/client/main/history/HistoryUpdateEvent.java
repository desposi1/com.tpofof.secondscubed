package com.tpofof.secondscubed.client.main.history;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.gwt.event.shared.GwtEvent;
import com.tpofof.secondscubed.client.main.timer.TimedSolveEvent;

public class HistoryUpdateEvent extends GwtEvent<HistoryUpdateEventHandler> {

	public static final Type<HistoryUpdateEventHandler> TYPE = new Type<HistoryUpdateEventHandler>();

	private final ImmutableList<TimedSolveEvent> currentHistory;
	
	public HistoryUpdateEvent(List<TimedSolveEvent> currentHistory) {
		super();
		this.currentHistory = ImmutableList.copyOf(currentHistory);
	}

	public ImmutableList<TimedSolveEvent> getCurrentHistory() {
		return currentHistory;
	}

	@Override
	public Type<HistoryUpdateEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(HistoryUpdateEventHandler handler) {
		handler.onHistoryUpdate(this);
	}
}
