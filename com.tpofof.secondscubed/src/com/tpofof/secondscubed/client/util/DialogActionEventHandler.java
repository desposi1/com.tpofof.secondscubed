package com.tpofof.secondscubed.client.util;

import com.google.gwt.event.shared.EventHandler;

public interface DialogActionEventHandler extends EventHandler {

	void onDialogActionEvent(DialogActionEvent event);
}
