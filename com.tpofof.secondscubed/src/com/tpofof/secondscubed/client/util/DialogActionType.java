package com.tpofof.secondscubed.client.util;

public enum DialogActionType {

	OPEN,
	CLOSE;
}
