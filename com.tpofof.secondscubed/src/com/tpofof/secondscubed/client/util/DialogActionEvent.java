package com.tpofof.secondscubed.client.util;

import com.google.gwt.event.shared.GwtEvent;

public class DialogActionEvent extends GwtEvent<DialogActionEventHandler> {

	public static final Type<DialogActionEventHandler> TYPE = new Type<DialogActionEventHandler>();
	
	private final DialogActionType actionType;	
	
	public DialogActionEvent(DialogActionType actionType) {
		super();
		this.actionType = actionType;
	}

	public DialogActionType getActionType() {
		return actionType;
	}

	@Override
	public Type<DialogActionEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(DialogActionEventHandler handler) {
		handler.onDialogActionEvent(this);
	}

}
