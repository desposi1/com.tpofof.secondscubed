package com.tpofof.secondscubed.datastore;

import java.util.List;

import com.tpofof.secondscubed.shared.SCUser;
import com.tpofof.secondscubed.shared.TimedSolve;

public interface DatastoreManager {

	SCUser saveUser(SCUser user);
	
	SCUser getUser(Long key);
	
	SCUser getUser(String email);
	
	SCUser deleteUser(SCUser user);

	TimedSolve saveTimedSolve(TimedSolve timedSolve);
	
	List<TimedSolve> getTimedSolvesByUserEmail(String user);
	
	TimedSolve deleteTimedSolve(TimedSolve timedSolve);
}
