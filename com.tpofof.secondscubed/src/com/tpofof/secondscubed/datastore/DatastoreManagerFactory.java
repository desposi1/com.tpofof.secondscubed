package com.tpofof.secondscubed.datastore;

public final class DatastoreManagerFactory {

	private static final GwtDatastoreManager GWT_DATASTORE_MANAGER = new GwtDatastoreManager();
	private static final DatastoreManager DEFAULT_DATASTORE_MANAGER = GWT_DATASTORE_MANAGER;
	
	private DatastoreManagerFactory() { }
	
	public static GwtDatastoreManager getGwtDatastoreManager() {
		return GWT_DATASTORE_MANAGER;
	}
	
	public static DatastoreManager getDefaultDatastoreManager() {
		return DEFAULT_DATASTORE_MANAGER;
	}
}
