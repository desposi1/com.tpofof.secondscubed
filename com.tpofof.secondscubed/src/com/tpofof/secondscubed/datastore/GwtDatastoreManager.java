package com.tpofof.secondscubed.datastore;

import java.util.List;
import java.util.Set;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.common.collect.Lists;
import com.tpofof.secondscubed.shared.SCUser;
import com.tpofof.secondscubed.shared.TimedSolve;

public class GwtDatastoreManager implements DatastoreManager {

	private final DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

	/*=====================
	 *  	SCUser
	 *=====================*/
	private static final String SCUSER_TYPE_INDEX = "SCUser";
	private static final String EMAIL_INDEX = "email";
	private static final String NICKNAME_INDEX = "nickanme";
	
	/*=====================
	 *  	TimedSolve
	 *=====================*/
	private static final String TIMED_SOLVE_INDEX = "TimedSolve";
	private static final String USER_EMAIL_INDEX = "userEmail";
	private static final String START_TIME_INDEX = "startTime";
	private static final String END_TIME_INDEX = "endTime";
	private static final String DIFF_INDEX = "diff";
	private static final String SCRAMBLE_INDEX = "scramble";
	private static final String TAGS_INDEX = "tags";
	
	@Override
	public SCUser saveUser(SCUser user) {
		Entity ent = new Entity(SCUSER_TYPE_INDEX);
		ent.setProperty(EMAIL_INDEX, user.getEmail());
		ent.setProperty(NICKNAME_INDEX, user.getNickname());
		Key key = datastore.put(ent);
		return key == null ? null : new SCUser(user.getNickname(), user.getEmail(), key.getId());
	}
	
	@Override
	public SCUser getUser(Long key) {
		try {
			return u(datastore.get(KeyFactory.createKey(SCUSER_TYPE_INDEX, key)));
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public SCUser getUser(String email) {
		Query.Filter filter = new Query.FilterPredicate(EMAIL_INDEX, Query.FilterOperator.EQUAL, email);
		Query q = new Query(SCUSER_TYPE_INDEX).setFilter(filter);
		Entity ent = datastore.prepare(q).asSingleEntity();
		return u(ent);
	}
	
	private SCUser u(Entity ent) {
		return ent == null ? null : new SCUser((String)ent.getProperty(EMAIL_INDEX),
				(String)ent.getProperty(NICKNAME_INDEX),
				ent.getKey().getId());
	}

	@Override
	public SCUser deleteUser(SCUser user) {
		if (user != null) {
			datastore.delete(KeyFactory.createKey(SCUSER_TYPE_INDEX, user.getKey()));
			return user;
		}
		return null;
	}

	@Override
	public TimedSolve saveTimedSolve(TimedSolve timedSolve) {
		Entity ent = new Entity(TIMED_SOLVE_INDEX);
		ent.setProperty(USER_EMAIL_INDEX, timedSolve.getUserEmail());
		ent.setProperty(START_TIME_INDEX, timedSolve.getStartTime());
		ent.setProperty(END_TIME_INDEX, timedSolve.getEndTime());
		ent.setProperty(DIFF_INDEX, timedSolve.getDiff());
		ent.setProperty(SCRAMBLE_INDEX, timedSolve.getScramble());
		ent.setProperty(TAGS_INDEX, timedSolve.getTags());
		Key key = datastore.put(ent);
		return key == null ? null : new TimedSolve(key.getId(), timedSolve);
	}

	@Override
	public List<TimedSolve> getTimedSolvesByUserEmail(String userEmail) {
		Query.Filter filter = new Query.FilterPredicate(USER_EMAIL_INDEX, Query.FilterOperator.EQUAL, userEmail);
		PreparedQuery pq = datastore.prepare(new Query(TIMED_SOLVE_INDEX).setFilter(filter));
		return ts(pq.asIterable());
	}

	@Override
	public TimedSolve deleteTimedSolve(TimedSolve timedSolve) {
		if (timedSolve != null) {
			datastore.delete(KeyFactory.createKey(TIMED_SOLVE_INDEX, timedSolve.getKey()));
			return timedSolve;
		}
		return null;
	}
	
	private List<TimedSolve> ts(Iterable<Entity> entities) {
		List<TimedSolve> solves = Lists.newArrayList();
		for (Entity e : entities)
			solves.add(ts(e));
		return solves;
	}
	
	@SuppressWarnings("unchecked")
	private TimedSolve ts(Entity ent) {
		return ent == null ? null
				: new TimedSolve(ent.getKey().getId(), 
						(String)ent.getProperty(USER_EMAIL_INDEX), 
						(Long)ent.getProperty(START_TIME_INDEX), 
						(Long)ent.getProperty(END_TIME_INDEX), 
						(Long)ent.getProperty(DIFF_INDEX), 
						(String)ent.getProperty(SCRAMBLE_INDEX), 
						(Set<String>)ent.getProperty(TAGS_INDEX));
	}
}
